<%@ page import="java.util.UUID" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>JSP Attributes</title>
</head>
<body>
<h1>HTTP Headers</h1>

<table>
    <c:forEach var="headerVar" items="${pageContext.request.headerNames}">
        <c:url var="headerLink" value="header.jsp">
            <c:param name="headerName" value="${headerVar}"/>
        </c:url>
        <tr>
            <td>${headerVar}</td>
            <td>${header[headerVar]}</td>
            <td><a href="${headerLink}">${headerLink}</a></td>
        </tr>
    </c:forEach>
</table>

<h1>Cookies</h1>
<form action="index.jsp" method="post">
    Nowy obiekt <input name="newCookie" type="text">
    <input type="submit" value="Zapisz">
</form>

<c:if test="${not empty param.newCookie}">
    <%
        Cookie cookie = new Cookie(UUID.randomUUID().toString(), request.getParameter("newCookie"));
        response.addCookie(cookie);
    %>
</c:if>

<%
    String deleteCookieName = request.getParameter("deleteCookieName");
    if (deleteCookieName != null) {
        Cookie cookie = new Cookie(deleteCookieName,"");
        cookie.setMaxAge(0);
        response.addCookie(cookie);
    }
%>

<table>
    <c:forEach var="cookieVar" items="${cookie}">
        <c:url var="deleteCookie" value="index.jsp">
            <c:param name="deleteCookieName" value="${cookieVar.key}"/>
        </c:url>
        <tr>
            <td>${cookieVar.key}</td>
            <td>${cookieVar.value.value}</td>
            <td><a href="${deleteCookie}">Delete</a></td>
        </tr>
    </c:forEach>
</table>

<h1>Session</h1>

<form action="index.jsp" method="get">
    Nowy obiekt <input name="newSessionObject" type="text">
    <input type="submit" value="Zapisz">
</form>

<c:if test="${not empty param.newSessionObject}">
    <% session.setAttribute(UUID.randomUUID().toString(), request.getParameter("newSessionObject")); %>
</c:if>

<table>
    <c:forEach var="sessionItem" items="${sessionScope}">
        <tr>
            <td>${sessionItem.key}</td>
            <td>${sessionItem.value}</td>
        </tr>
    </c:forEach>
</table>
</body>
</html>
